import React, {Fragment, useEffect} from 'react'
import {Header, Flex, Line, Title, About, Journey, Experience, Project, Card, Contact} from './styles'
import Navbar from '../Navbar'
import Aos from 'aos' 
import "aos/dist/aos.css"
import { animateScroll as scroll} from 'react-scroll'
import Profile from './Media/profile.png'
import Study from './Media/study.png'
import ArrowDown from './Media/arrow-down3.png'
import Ping from './Media/ping.png'
import Linkedin from './Media/linkedin.png'
import Instagram from './Media/instagram.png'
import Mipaui from "./Media/mipaui.png"

const Homepage = () => {

    useEffect(() => {
        Aos.init({duration: 2000})
    }, [])

    return(
        <Fragment style={{scrollBehavior: 'smooth'}}>
            <Navbar />
            <Header>
                <Flex direction="row" >
                    <Flex direction="column" >
                        <Flex direction="row">
                            <h2 className="hello">Hello</h2>
                            {/* <div style={{width: '330px', height: '2px'}} /> */}
                        </Flex>
                        <h1 className="name">I AM Muhammad Albar</h1>
                        <h2 className="study">Undergraduate Computer Science Student</h2>
                    </Flex>
                    <Flex direction="column">
                        <img src={Profile} className="profile" alt="profile_pic" />
                    </Flex>
                </Flex>
     
                    <img src={ArrowDown} className="arrow" alt="arrowdown" onClick={() => scroll.scrollTo(1000)} />
              
            </Header>

            <About  data-aos="fade-up" >
           
                    <Title>About Me</Title>
                    <Line width="90em" />
                    <Flex direction="row">
                        <img src={Study} alt="study" />
                        <p className="text">
                            Heyo, I’m Muhammad - undergraduate computer science student. I was born and raised in Jakarta. Before collage, i never really had a spesific career goal, 
                            funnily I just wanted to be the next creator of cool things, like Tony Stark. I felt like computer science was the only way to go so i decided to pursue a 
                            degree in Computer Science (CS).
                            <br /><br />
                            I am also an energetic person who has developed a mature and responsible approach to any task that i undertake, or situation that i am presented with. As a Computer Science student, i'd love to learn anything new about technology and look forward to work with the others in the industry. 
                        
                        </p>
                    </Flex>
                
            </About>

            <Journey id="journey" data-aos="fade-up">
                <Title>My Journey</Title>
                <Flex direction="row" >
                    <Flex direction="column" className="education">
                        <h1>Education</h1>
                        <div>
                            <h2>Universitas Indonesia</h2>
                            <p>Majoring in Computer Science - 2017</p>
                        </div>
                    </Flex>
                    <Flex direction="column" className="voulenteer">
                        <h1>Voulenteer Experience</h1>
                        <div>
                            <h2>Pemira</h2>
                            <p>Pemungutan Suara - 2017</p>
                        </div>
                        <div>
                            <h2>Pembinaan Mahasiswa Baru</h2>
                            <p>Mentor - 2018</p>
                        </div>
                        <div>
                            <h2>Compfest</h2>
                            <p>Trasnportaion and Venue - 2018</p>
                        </div>
                    </Flex>
                </Flex>
            </Journey>

            <Experience id="experience" data-aos="fade-down">
                <Title>Experience</Title>
                <Flex direction="row" wrap="wrap" mobile={true}>
                    <Flex direction="column" className="employment">
                        <h1>Employment History</h1>
                        <div>
                            <h1>Qlue Smart City</h1>
                            <h2 className="role">Frontend Engineer Intern</h2>
                            <h2 className="time">June 2020 - September 2020</h2>
                            <p>Responsible in implementing Customer Relationship Management for Qlue Dashboard</p>
                        </div>
                    </Flex>
                    <Flex direction="column" className="skills">
                        <h1>Skills</h1>
                        <Flex direction="row">
                            <Flex direction="column" >
                                <h3>HTML</h3>
                                <h3>Javascript</h3>
                                <h3>Postgres</h3>
                            </Flex>
                            <Flex direction="column" >
                                <h3>CSS</h3>
                                <h3>ReactJS</h3>
                                <h3>AWS</h3>
                            </Flex>
                            <Flex direction="column" >
                                <h3>NodeJS</h3>
                                <h3>Python</h3>
                                <h3>Django</h3>
                            </Flex>
                        </Flex>
                        
                    </Flex>
                </Flex>
            </Experience>

            <Project id="project">
                <Title>Project</Title>
                <Flex direction="row" justify="space-around" style={{marginTop: '5em'}} mobile={true}>
                    <Card>
                        <h1>Ping! Auction</h1>
                        <img src={Ping} alt="Ping! Auction" />
                    </Card>
                    <Card>
                        <h1>BEM FMIPA UI</h1>
                        <img src={Mipaui} alt="Bem FMIPA UI" />
                    </Card>
                </Flex>
                <Flex direction="row" justify="center">
                    <a href="https://docs.google.com/document/d/1dVcv4VItiJvfeurdVW7SKeaxu6MgpYNtoelJtubukx4/edit?usp=sharing" className="button1">Resume</a>
                </Flex>
            </Project>

            <Contact id="contact">
                <h1>Muhammadalbr@gmail.com</h1>
                <h2>Always down to collaborate when I have the time. I'm hoping to further my career by disrupting the social product space. Let's get in touch ✌️.</h2>
                <Flex direction="row">
                    <a href="https://www.linkedin.com/in/muhammad-albar-5057bb193/">
                        <img src={Linkedin} alt="linkedin_profile" />
                    </a>
                    <a href="https://www.instagram.com/muhammadalbr" style={{marginLeft: '5em'}}>
                        <img src={Instagram} alt="instagram_profile" />
                    </a>
                    
                </Flex>
            </Contact>
        </Fragment>
        
    )
}

export default Homepage
import styled from 'styled-components'

export const Flex = styled.div`
    display: flex;
    flex-direction: ${({direction}) => direction};
    justify-content: ${({justify}) => justify};
    align-items: ${({align}) => align};
    wrap: ${({wrap}) => wrap};
    @media only screen and (max-width:768px){
        ${({mobile})=>mobile && `
        flex-direction:column;`}
    }
    
`

export const Wrapper = styled.div`
    text-align: center;
    margin-top: 100px;
`

export const Header = styled.div`
    color: black;
    font-weight: bold;
    margin-top: 70px;
    margin-left: 15em;
   
    
    .hello{
        font-size: 36px;
        margin-top: 6em;
        
    }
    .name{
        overflow: hidden;
        white-space: nowrap;
        font-size: 48px;
        animation: 
        typing 3s steps(40, end),
        blink-caret .75s step-end infinite;

        @keyframes typing {
            from { width: 0 }
            to { width: 100% }
        }

        @keyframes blink-caret {
            from, to { border-color: transparent }
            50% { border-color: orange; }
        }
        
    }
    .study{
        font-size: 30px;
    }
    img{
        margin-left: 20em;
        margin-top: 4em;
    }
    .profile{
        display: block;
    }
    .arrow{
        width: 5em;
        margin-left: 38%;
        cursor: pointer;
        -webkit-animation: mover 1s infinite  alternate;
        animation: mover 1s infinite  alternate;
    }
    @-webkit-keyframes mover {
        0% { transform: translateY(0); }
        100% { transform: translateY(-20px); }
    }
    @keyframes mover {
        0% { transform: translateY(0); }
        100% { transform: translateY(-20px); }
    }

    @media (max-width: 1280px){
        margin-left: 5em;
        img{
            margin-left: 10em;
        }
    }
    @media (max-width: 414px){
        margin-left: 2em;
        .hello{
            font-size: 24px;
        }
        .name{
            font-size: 30px;
        }
        .study{
            font-size: 24px;
        }
        .profile{
            display: none;
        }
    }
`

export const Line = styled.div`
    width: ${({width}) => width};
    height: 2px;
    background: black;

    @media(max-width: 1280px){
        width: 60em;
    }
    @media (max-width: 414px){
        width: 15em;
    }
`

export const Title = styled.p`
    font-size: 36px;
    font-weight: bold;
    color: black;
    
    @media (max-width: 414px){
        font-size: 24px;
    }
`

export const About = styled.div`
    width: 89.9%;
    height: 650px;
    background: rgba(0, 0, 0, 0.05);
    margin-top: 20em;
    padding: 3em 6em;

    .title{
        font-size: 36px;
        font-weight: bold;
        color: black;
    }
    img{
        display: block;
        width: 35em;
        margin-top: 2em;
    }
    .text{
        font-size: 24px;
        padding: 4em 4em;
        weight: 400;
        line-height: 24px;
        width: 40em;
        text-align: justify;
        color: black;
        
    }

    @media (max-width: 1280px){
        width: 84.8%;
        img{
            width: 27em;
            height: 30em;
        }
        .text{
            font-size: 20px;
            // margin: 5em 7em;
        }

    }
    @media (max-width: 414px){
        padding: 1em 2em;
        img{
            display: none;
        }
        .text{
            font-size: 15px;
            padding: 0;
            width: 20em;
        }
    }
`

export const Journey = styled.div`
    margin-top: 100px;
    padding: 3em 6em;

    .voulenteer{
        margin-left: 48em;
    }

    h1{
        font-size: 30px;
        weight: 700;
    }
    div{
        margin: 0;
        h2{
            font-size: 30px;
            weight: 400;
        }
        p{
            font-size: 25px;
            weight: 400;
            color: #757575;
        }

    }

    @media (max-width: 1280px){
        .voulenteer{
            margin-left: 17em;
        }
       
    }
    @media (max-width: 414px){
        margin-left: -4em;
        .education{
            h1{
                font-size: 22px;
            }
            h2{
                font-size: 20px;
            }
            p{
                font-size: 18px;
            }
        }
        .voulenteer{
            margin-left: 7em;
            h1{
                font-size: 22px;
            }
            h2{
                font-size: 20px;
            }
            p{
                font-size: 18px;
            }
        }
    }
    
`

export const Experience = styled.div`
    margin-top: 100px;
    padding: 3em 6em;
    h1{
        font-size: 30px;
        weight: 700;
    }
    div{
        margin: 0;
        .role{
            font-size: 25px;
            weight: 400;
        }
        .time{
            font-size: 25px;
            font-weight: regular;
        }
        p{
            font-size: 25px;
            weight: 400;
            color: #757575;
            width: 530px;
        }

    }
    h3{
        font-size: 25px;
        weight: 700;
        text-align: center;
        padding: 10px;
        margin-left: 20px;
        border-radius 10px;
        background: black;
        color: white;
    }
    .skills{
        margin-left: 40em;
    }
    @media (max-width: 1280px){
        .skills{
            margin-left: 10em;
        }
        
    }
    @media (max-width: 414px){
        padding: 2em 2em;
        
        .employment{
            h1{
                font-size: 24px;
            }
            h2{
                font-size: 20px;
            }
            p{
                font-size: 16px;
                width: 19em;
            }
        }
        .skills{
            margin-left: 0;
        }
        h3{
            font-size: 20px;
            margin-left: 0.5em;
        }
    }

`;

export const Project = styled.div`
    margin-top: 100px;
    padding: 3em 6em;
    img{
        width: 32em;
        height: 38em;
    }

    a.button1{
        display:inline-block;
        margin-top: 5em;
        padding:1.2em 7.2em;
        border: 0.2em solid black;
        border-radius:0.12em;
        box-sizing: border-box;
        text-decoration:none;
        font-family:'Roboto',sans-serif;
        font-weight: 500;
        font-size: 20px;
        color: black;
        text-align:center;
        transition: all 0.3s;
    }
    a.button1:hover{
        color: white;
        background-color: black;
    }

    @media (max-width: 1280px){
        img{
            width: 28em;
            height: 33em;
        }
    }
    @media (max-width: 414px){
        padding: 2em 2em;
        img{
            width: 19em;
            height: 23em;
        }
        a.button1{
            padding: 1em 6em;
        }
    }

`

export const Card = styled.div`
    width: 538px;
    height: 721px;
    text-align: center;
    align-items: center;
    background: white;
    box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
    :hover{
        box-shadow: 0px 7px 20px rgba(0, 0, 0.2, 0.25);
        transition: 0.4s;
    }
    // filter: drop-shadow(0px 15px 13px rgba(0, 0, 0, 0.22)), drop-shadow(0px 19px 38px rgba(0, 0, 0, 0.3));

    @media (max-width: 1280px){
        width: 470px;
        height: 625px;
    }
    @media (max-width: 414px){
        width: 335px;
        height: 490px;
        margin-top: 10px;
    }
    
`

export const Contact = styled.div`
    margin-top: 100px;
    padding: 3em 6em;
    h1{
        font-size: 60px;
        weight: 900;
    }
    h2{
        font-size: 30px;
        font-weight: normal;
        width: 37em;
    }
    img{
        width: 70px;
    }

    @media (max-width: 1280px){
        h1{
            font-size: 45px;
        }
        h2{
            font-size: 24px;
        }
        img{
            width: 50px;
        }
    }
    @media (max-width: 414px){
        padding: 2em 2em;
        h1{
            font-size: 28px;
        }
        h2{
            font-size: 20px;
            width: 18em;
        }
    }
`
import styled from 'styled-components'

export const Wrapper = styled.div`
    *{
        padding: 0;
        margin: 0;
        text-decoration: none;
        list-style: none;
        box-sizing: border-box;
     
    }
    
   
    .navbar{
        background: white;
        top: 0;
        position: fixed;
        height: 100px;
        width: 100%;
        z-index: 999;
    }
    .navbar.active{
        background: #26262C;
        a{
            color: white;
        }
        transition: 0.3s;
    }
    nav ul{
        float: right;
        margin-right: 3em;
    }
    nav ul li{
        display: inline;
        line-height: 80px;
        margin: 0 40px;
    }
    nav ul li a{
        color: black;
        weight: 500;
        cursor: pointer;
    }
    a:hover{
        border-bottom: 3px solid blue;
        transition: 0.3s;
        transition-timing-function: ease;
    }
    .checkbtn{
        font-size: 30px;
        float: right;
        line-height: 3em;
        margin-right: 2em;
        cursor: pointer;
        display: none;
    }
    #check{
        display: none;
    }

    @media (max-width: 768px){
        .navbar{
            background: #26262C;
            width: 100%;  
        }
        .checkbtn{
            display: block;
            color: white;
        }
        ul{
            position: fixed;
            width: 100%;
            height: 100vh;
            background: #26262C;
            top: 5em;
            left: -36em;
            text-align: center;
            transition: all 0.5s;
        }
        nav ul li{
            display: block;
            margin: 50px 0;
            line-height: 30px;
        }
        nav ul li a{
            color: white;
        }
        #check:checked ~ ul{
            left: 0;
        }
    }

`
import React, {useState} from 'react'
import {Wrapper} from './styles'
import { animateScroll as scroll } from 'react-scroll'
import 'font-awesome/css/font-awesome.min.css';

const Navbar = () => {
    const [navbar, setNavbar] = useState(false)
    const [screen, setScreen] = useState(false)
    const changeBackground = () => {
        if(window.scrollY >= 100){
            setNavbar(true)

        }
        else{
            setNavbar(false)
        }
    }
    window.addEventListener('scroll', changeBackground)

    const scrollPosition = (position) => {
        if(window.screen.width <= 414){
            setScreen(true)
            scroll.scrollTo(position)
        }
        else{
            setScreen(false)
            scroll.scrollTo(position)
        }
    }


    return(
        <Wrapper>
            <nav className={navbar ? 'navbar active': 'navbar'}>
                <input type="checkbox" id="check" />
                <label for="check" className="checkbtn">
                    <i className="fa fa-bars" />
                </label>
                <ul>
                    <li><a onClick={() => scroll.scrollToTop()}>Home</a></li>
                    <li><a onClick={() => screen ? scrollPosition(800) : scrollPosition(1000)}>About</a></li>
                    <li><a onClick={() => screen ? scrollPosition(1600) : scrollPosition(1900)}>Journey</a></li>
                    <li><a onClick={() => screen ? scrollPosition(2400) : scrollPosition(2600)}>Experience</a></li>
                    <li><a onClick={() => scroll.scrollToBottom()}>Contact</a></li>
                </ul>
            </nav>
            
        </Wrapper>
    )
}

export default Navbar